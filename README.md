# BNP Task

1.Clone this Repository in your local repository.
[ git clone https://gitlab.com/harisathi01/bnp-task.git  ]

2. import the project in eclipse or anyother IDE.

3. Run the Main Class.

4. Use PostMan API Platform to Test the API.
    - Select Post Method
    - Enter URL [ localhost:8080/result ]
    - Select Body >> raw >> choose the format as JSON
    - Enter this input    
        [
        [1, 2, 3],
        [4, 5, 9],
        [7, 8, 9]
        ]

    - Click Send Button to get the Result 

    - output : [ 1,2,3,9,9,8,7,4,5]

