package com.bnp.task.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArrayController {

	@PostMapping("/result")
	public List<Integer> arrangeArrayClockwise(@RequestBody int[][] array) {
		int rows = array.length;
		int cols = array[0].length;
		int top = 0;
		int bottom = rows - 1;
		int left = 0;
		int right = cols - 1;
		
		 List<Integer> result = new ArrayList<>();

	        while (top <= bottom && left <= right) {
	            
	        	// to get top row numbers
	            for (int i = left; i <= right; i++) {
	                result.add(array[top][i]);
	            }
	            top++;
	            
	            //to get right column numbers
	            for (int i = top; i <= bottom; i++) {
	                result.add(array[i][right]);
	            }
	            right--;
	            
	            // to get bottom row numbers
	            if (top <= bottom) {
	                for (int i = right; i >= left; i--) {
	                    result.add(array[bottom][i]);
	                }
	                bottom--;
	                
	                // to get left column numbers
	                if (left <= right) {
	                    for (int i = bottom; i >= top; i--) {
	                        result.add(array[i][left]);
	                    }
	                    left++;
	                }
	            }
	        }
		return result;

	}
}
